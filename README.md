<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[travis-image]: https://api.travis-ci.org/nestjs/nest.svg?branch=master
[travis-url]: https://travis-ci.org/nestjs/nest
[linux-image]: https://img.shields.io/travis/nestjs/nest/master.svg?label=linux
[linux-url]: https://travis-ci.org/nestjs/nest
  
  <p align="center">A progressive <a href="http://nodejs.org" target="blank">Node.js</a> framework for building efficient and scalable server-side applications, heavily inspired by <a href="https://angular.io" target="blank">Angular</a>.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/dm/@nestjs/core.svg" alt="NPM Downloads" /></a>
<a href="https://travis-ci.org/nestjs/nest"><img src="https://api.travis-ci.org/nestjs/nest.svg?branch=master" alt="Travis" /></a>
<a href="https://travis-ci.org/nestjs/nest"><img src="https://img.shields.io/travis/nestjs/nest/master.svg?label=linux" alt="Linux" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#5" alt="Coverage" /></a>
<a href="https://gitter.im/nestjs/nestjs?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=body_badge"><img src="https://badges.gitter.im/nestjs/nestjs.svg" alt="Gitter" /></a>
<a href="https://opencollective.com/nest#backer"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec"><img src="https://img.shields.io/badge/Donate-PayPal-dc3d53.svg"/></a>
  <a href="https://twitter.com/nestframework"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Basic JavaScript/TypeScript + Algorithm
```bash
1. Fibonacci Sequence: Write a function fib that return the value of n-th order of fibonacci sequence.
   In mathematics, the Fibonacci numbers are the numbers in the following integer sequence, called the Fibonacci sequence, and characterized by the fact that every number after the first two is the sum of the two preceding ones:
   
   answers: src/domain/joke/joke.service.ts private fibonacci(n: number, memo: Map<number, number> = new Map()) // บรรทัดที่ 101  init constructor() บรรทัดที่ 25, 26, 27
2. Array shift: Write a function shift that shifts the elements of array to left or right by n elements in an infinite loop.
   
   answers: src/domain/joke/joke.service.ts private static shift(arr: any[], direction: any, n: any) // บรรทัดที่ 153  init constructor() บรรทัดที่ 36, 37
   
3. Second max: Write a function secondMax that receive an array of number. The function will return the second maximum value of the array. If there is no second max, return max instead. If an array is empty, throw and error.

   answers: src/domain/joke/joke.service.ts private static secondMax(arr: any[]) // บรรทัดที่ 130  init constructor() บรรทัดที่ 31, 32, 33, 34, 35
   
4. FizzBuzz...But: You may heard FizzBuzz task. Here we have the same rule. You will write a function fizzBuzz that receive a single parameter it will return the value base on these rule.
   If the input is divisable by 3, return 'Fizz'
   If the input is divisable by 5, return 'Buzz'
   If the input is divisable by both 3 and 5, return 'FizzBuzz'
   
   answers: src/domain/joke/joke.service.ts private static fizzbuzz(n: number) // บรรทัดที่ 117  init constructor() บรรทัดที่ 28, 29, 30
```


## Back-end Questions
Explain First-party cookie & Third-party cookie
   - First-party 
   คุกกี้ของบุคคลที่หนึ่งถูกสร้างขึ้นโดยโดเมนโฮสต์ - โดเมนที่ผู้ใช้กําลังเข้าชม โดยทั่วไปคุกกี้ประเภทนี้ถือว่าดี พวกเขาช่วยให้ประสบการณ์การใช้งานที่ดีขึ้นและเปิดเซสชั่น โดยทั่วไปหมายถึงเบราว์เซอร์สามารถจดจําข้อมูลสําคัญเช่นรายการที่คุณเพิ่มในรถเข็น, ชื่อผู้ใช้และรหัสผ่านของคุณและการตั้งค่าภาษา
   
   - Third-party
   คุกกี้ของบุคคลที่สามคือคุกกี้ที่สร้างโดยโดเมนอื่นนอกเหนือจากที่ผู้ใช้เข้าชมในเวลานั้น และส่วนใหญ่จะใช้สําหรับการติดตามและวัตถุประสงค์ในการโฆษณาออนไลน์ พวกเขายังอนุญาตให้เจ้าของเว็บไซต์ให้บริการบางอย่างเช่นการแชทสด
    

Explain CAP Theorem.
  - Consistency
  ระบบจะกล่าวว่าสอดคล้องถ้าโหนดทั้งหมดเห็นข้อมูลเดียวกันในเวลาเดียวกันเพียงถ้าเราดําเนินการอ่านบนระบบที่สอดคล้องกันก็ควรจะส่งกลับค่าของการดําเนินการเขียนล่าสุด ซึ่งหมายความว่าการอ่านควรทําให้โหนดทั้งหมดเพื่อส่งกลับข้อมูลเดียวกันคือค่าของการเขียนล่าสุด
  
  - Availability
  ความพร้อมใช้งานในระบบแบบกระจายช่วยให้มั่นใจได้ว่าระบบยังคงใช้งานได้ 100% ของเวลา ทุกคําขอได้รับการตอบสนอง (ไม่ใช่ข้อผิดพลาด) โดยไม่คํานึงถึงแต่ละรัฐของโหนด
  
  - Partition Tolerance
  เงื่อนไขนี้ระบุว่า ระบบไม่ล้มเหลว โดยไม่คํานึงถึงว่าข้อความจะถูกลบหรือล่าช้าระหว่างโหนดในระบบ
  
  ความอดทนพาร์ทิชันได้กลายเป็นความจําเป็นมากกว่าตัวเลือกในระบบกระจาย มันเป็นไปได้โดยการจําลองแบบพอระเบียนข้ามการรวมกันของโหนดและเครือข่าย
  

Considering two queries

```bash
const searchIds = ['1', '2', '3', ...];

const query1 = await Product.find({ id: { $in: searchIds } });

const query2 = await Promise.all(searchIds.map(searchId => Product.find({ id: searchId })));

answers: query2

```

Explain how HTTP protocol works.
 แบ่งออกเป็น http Request กับ http Response
 - Request
 http request จะกำหนด action ที่จะกระทำต่อข้อมูล method จะมีลักษณะเป็นตัวพิมพ์ใหญ่ทั้งหมดเสมอ
 โดย method ที่สำคัญมี 4 ตัวคือ GET, POST, PUT, DELETE
 - GET — เป็นการเรียกรับข้อมูลจาก URI ที่กำหนด method GET ควรใช้ในการดึงข้อมูลเท่านั้นและต้องไม่มีผลกระทบใด ๆ กับข้อมูล
 - POST — ใช้สำหรับการสร้างข้อมูลใหม่โดยส่งข้อมูลผ่าน body
 - PUT — ใช้สำหรับแทนที่ข้อมูลที่มีทั้งหมดด้วยข้อมูลใหม่ที่ส่งขึ้นไป
 - DELETE — ใช้สำหรับลบข้อมูลที่มีอยู่ ของเป้าหมายที่กำหนดโดย URI
 
 - Response
 การ Response จะเกิดขึ้นหลังจากส่ง Request ไปที่ server แล้ว server ก็จะมีการตอบรับกลับมาซึ่งเราจะเรียกข้อความที่ตอบกลับมาว่า http-Response message
 http status-code จะแบ่งออกเป็นหมวดหมู่ตามเลขที่อยู่ตัวหน้าสุด และที่ยกมาไว้จะเป็น Response ที่มักจะเจอ โดย Response อื่น ๆ สามารถไปอ่านต่อได้ใน Status Code Definitions
 2xx (สำเร็จ) หมายความว่าการ request นั้นได้รับแล้วและกระทำตาม method สำเร็จโดย Server
 - 200 Ok เป็นมาตรฐานของ HTTP Response นั้น Success สำหรับ GET, PUT หรือ POST
 - 201 Create เป็น Response สำหรับข้อมูลใหม่ได้ถูกสร้างขึ้น ใช้สำหรับ POST
 -204 No Content เป็น Response สำหรับ request ที่ดำเนินการ Success แต่ไม่ได้ return ข้อมูลกลับ
 3xx (Redirection)
 - 304 Not Modified เป็น status code ที่บอกว่า client ได้รับการ Response แล้วอยู่ใน cache และไม่จำเป็นจะต้องส่งผ่านข้อมูลเดิมอีกครั้ง
 4xx (Client error) โดย status ในกลุ่มนี้จะบอก client ว่า request ที่เข้ามา error
 - 400 Bad Request บอกว่า request ที่ส่งมาโดย client นั้นไม่ถูกดำเนินการ และ Server ไม่เข้าใจว่า request เกี่ยวกับอะไร
 - 401 Unauthorized บอกว่า client ไม่ได้รับอนุญาตในการเข้าถึงข้อมูลและควรจะส่ง credential มาพร้อม request
 - 403 Forbidden บ่งบอกว่า request นั้นถูกต้องและ client ได้รับการอนุญาต แต่ Client ไม่ได้รับการอนุญาตให้เข้าถึงข้อมูลด้วยเหตุผลบางประการ
 - 404 Not Found บ่งบอกว่า resource ที่ request มานั้น ไม่ว่างใช้งานตอนนี้
 - 405 Gone บ่งบอกว่าข้อมูลที่ต้องการนั้นไม่มีอยู่แล้ว หรืออาจจะย้ายไปที่อื่น
 5xx (Server error)
 - 500 Internal Server Error บอกว่าการ request นั้นถูกต้อง แต่ server มีความสับสนและจะบริการด้วยเงื่อนไขที่คาดการไม่ได้
 - 503 Service Unavailable บอกว่า server ใช้การไม่ได้ หรือไม่ว่างที่จะรับและดำเนินการ request โดยส่วนใหญ่แล้ว server อยู่ในช่วงบำรุงรักษา



## Back-End
```bash
# ใช้ swagger ในการทดลองใช้ API
# endpoint swagger localhost:3000/api
# Setup TEST API 
 1. POST /user/ Add new User. 
 2. PUT /user/:id/activate ** approved user.
 3. GET /login userame password
 4. POST /joke/ Add new joke. (Authentication Add Header 'x-profile': {token})
 5. GET /joke/ Get all jokes. (Authentication Add Header 'x-profile': {token})
 6. GET /joke/:id Get joke by id. (Authentication Add Header 'x-profile': {token})
 7. POST /joke/:id/like Like a joke. (Authentication Add Header 'x-profile': {token})
 8. DELETE /joke/:id Delete joke. (Authentication Add Header 'x-profile': {token})
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

  Nest is [MIT licensed](LICENSE).
